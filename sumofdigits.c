#include<stdio.h>
int input()
{
 int a;
 printf("Enter the number\n");
 scanf("%d",&a);
 return a;
}
int sumofdigits(int a)
{
 int b;
 int sum=0;
 while (a>0)
 {
  b=a%10;
  a=a/10;
  sum+=b;
 }
 return sum;
}
int output(int sum,int a)
{
 printf("The sum of digits of %d is %d\n",a,sum);
 return 0;
}
int main()
{
 int a,sum;
 a=input();
 sum=sumofdigits(a);
 output(sum,a);
 return 0;
} 